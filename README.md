# Explr

Explr is a community empowerment platform to promote domestic and international tourism across Africa. Through this program I hope to increase revenue for communities at tourist sites. This helps promote zero-hunger of the SDG.

## How To Use

```
git clone https://ankorgh@bitbucket.org/ankorgh/explr.git

cd explr

npm install

npm run start

open http://localhost:8081/index.html in your favourite browser
```

### Technologies

- React360
- React Native
- React
- CSS
