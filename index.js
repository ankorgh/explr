import React from 'react';
import { AppRegistry, StyleSheet, Text, View, VrButton, Image, NativeModules } from 'react-360';
import { Environment, asset } from 'react-360';
import staticAssets from './assets.json';

const AudioModule = NativeModules.AudioModule;

export default class explre extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCategory: 'main',
      locations: staticAssets.places,
      current: 0,
      placeTitle: ''
    };
  }

  componentDidMount() {}

  _showNextImage() {
    const position = this.state.current;
    const type = this.state.currentCategory;

    this._changeImage(this.state.locations[type][position]['url']);
    this.setState({ placeTitle: this.state.locations[type][position]['title'] });

    if (position === Object.keys(this.state.locations[type]).length - 1) {
      return this.setState({ current: 0 });
    }

    this.setState({ current: position + 1 });
  }

  _changeImage(url) {
    Environment.setBackgroundImage(asset(url), {
      format: '2D',
      transition: 1000
    });
  }

  _playBackgroundSound() {
    AudioModule.playEnvironmental({
      source: asset('brown-skill.mp3'),
      volume: 0.3
    });
  }

  _playSound(type) {
    let name = type === 'enter' ? 'switch-flip' : 'menu-click';
    AudioModule.playOneShot({
      source: asset(`${name}.wav`),
      volume: 0.75
    });
  }

  _moveToHome() {
    this._playSound('click');
    this.setState({ currentCategory: 'main' });
    this._changeImage('main.jpg');
    this.setState({ current: 0 });
    this.setState({ placeTitle: '' });
  }

  render() {
    if (this.state.currentCategory === 'main') {
      return (
        <View style={styles.panel}>
          <View style={styles.places}>
            <VrButton
              onClick={() => {
                this._playSound('click');
                this.setState({ currentCategory: 'castle' }, () => {
                  this._showNextImage();
                });
              }}
              onEnter={() => {
                this._playSound('enter');
              }}
            >
              <View style={styles.place}>
                <Image source={asset('castle.svg')} style={{ height: 150, width: 150 }}></Image>
                <Text style={styles.placeTitle}>Castles/Forts</Text>
              </View>
            </VrButton>

            <VrButton
              onClick={() => {
                this._playSound('click');
                this.setState({ currentCategory: 'zoo' }, () => {
                  this._showNextImage();
                });
              }}
              onEnter={() => {
                this._playSound('enter');
              }}
            >
              <View style={styles.place}>
                <Image source={asset('horse.svg')} style={{ height: 150, width: 150 }}></Image>
                <Text style={styles.placeTitle}>Zoo's</Text>
              </View>
            </VrButton>

            <VrButton
              onClick={() => {
                this._playSound('click');
                this.setState({ currentCategory: 'park' }, () => {
                  this._showNextImage();
                });
              }}
              onEnter={() => {
                this._playSound('enter');
              }}
            >
              <View style={styles.place}>
                <Image source={asset('park.svg')} style={{ height: 150, width: 150 }}></Image>
                <Text style={styles.placeTitle}>Parks</Text>
              </View>
            </VrButton>

            <VrButton
              onClick={() => {
                this._playSound('click');
                this.setState({ currentCategory: 'event' }, () => {
                  this._showNextImage();
                });
              }}
              onEnter={() => {
                this._playSound('enter');
              }}
            >
              <View style={styles.place}>
                <Image source={asset('museum.svg')} style={{ height: 150, width: 150 }}></Image>
                <Text style={styles.placeTitle}>Monuments</Text>
              </View>
            </VrButton>
          </View>
          <View style={styles.bits}>
            <Text style={styles.bitsTitle}>Bits Of Ghana</Text>
            <Text style={styles.bitsInfo}>Ghana is known to be the second highest producing cocoa in the world.</Text>
            <Text style={styles.bitsInfo}>Ghanaians are know to be affectionate, peace loving people.</Text>
            <Text style={styles.bitsInfo}>
              Ghana is blessed with a lot natural resources including cocoa, gold, copper, bauxite, and many more.
            </Text>
          </View>
          {/* <VrButton
            onClick={() => {
              this._playSound('enter');
              this._playBackgroundSound();
            }}
            onEnter={() => {
              this._playSound('enter');
            }}
            style={styles.jukebox}
          >
            <View>
              <Image source={asset('pot.png')} style={{ height: 80, width: 80 }}></Image>
            </View>
          </VrButton> */}
        </View>
      );
    }

    return (
      <View>
        <View style={styles.placeTitleContainer}>
          <Text style={styles.placeTitle}>{this.state.placeTitle}</Text>
        </View>

        <VrButton
          onClick={() => {
            this._moveToHome();
          }}
          onEnter={() => {
            this._playSound('enter');
          }}
        >
          <View style={styles.backMenu}>
            <Image source={asset('left.svg')} style={{ height: 25, width: 25, marginTop: 10 }}></Image>
            <Text style={styles.backMenuText}>Back To Menu</Text>
          </View>
        </VrButton>

        <VrButton
          onClick={() => {
            this._playSound('click');
            this._showNextImage();
          }}
          onEnter={() => {
            this._playSound('enter');
          }}
          style={styles.locator}
        >
          <Image source={asset('pot.png')} style={{ height: 120, width: 120 }}></Image>
        </VrButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  panel: {
    // Fill the entire surface
    width: 1000,
    height: 600,
    backgroundColor: 'rgba(255, 255, 255, 0.25)',
    // alignItems: 'center',
    flex: 1,
    flexDirection: 'row'
    // backgroundColor: '#777777'
  },
  places: {
    flexDirection: 'row',
    flex: 3,
    flexWrap: 'wrap'
  },
  place: {
    backgroundColor: 'rgba(255, 176, 0, 0.65)',
    height: 250,
    width: 250,
    marginLeft: 20,
    marginTop: 20,
    paddingLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    borderRadius: 5
  },
  jukebox: {
    flex: 1,
    alignSelf: 'center',
    marginLeft: 40
  },
  placeTitleContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderColor: 'rgba(255, 176, 0, 0.95)',
    borderWidth: 3,
    borderStyle: 'solid',
    borderRadius: 20,
    backgroundColor: 'rgba(255, 176, 0, 0.55)'
  },
  placeTitle: {
    fontSize: 80
  },
  bits: {
    marginLeft: 20,
    flex: 2
  },
  bitsTitle: {
    fontSize: 30,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  bitsInfo: {
    fontSize: 25,
    fontWeight: '400',
    marginLeft: 10
  },
  placeTitle: {
    fontSize: 30,
    textAlign: 'center',
    fontWeight: '500'
  },
  backMenu: {
    backgroundColor: 'rgba(0,21,20,0.45)',
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 5,
    transform: [{ translate: [700, -250, 0] }]
  },
  backMenuText: {
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 30
  },
  locator: {
    transform: [{ translate: [0, -200, 0] }]
  }
});

AppRegistry.registerComponent('explre', () => explre);
